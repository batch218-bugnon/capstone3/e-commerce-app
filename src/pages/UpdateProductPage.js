import { useState, useEffect } from 'react';
import { useParams,  useNavigate } from 'react-router-dom';
import { Card, Button, Form, Container}  from 'react-bootstrap';
import Swal from 'sweetalert2';



export default function UpdateProductPage(e) {
    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [price, setPrice] = useState('');

    const { productId } = useParams();

    const navigate = useNavigate();

    function updateProduct(e) {
        e.preventDefault()

        fetch(`${process.env.REACT_APP_API_URL}/product/update/${productId}`, {
            method: 'PATCH',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                name: name,
                description: description,
                price: price
            })
        })
        .then(res => res.json())
        .then(data => {
            if(data === true) {
                Swal.fire({
                    title: 'Product Successfully Updated',
                    icon: 'success',
                });

                navigate('/products');

                // Programmatic route back to dashboard.
            } else {
                Swal.fire({
                    title: 'Something went wrong!',
                    icon: 'error',
                    text: 'Please try again or contact your administrator.'
                });
            }
        })
    }

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/product/${productId}`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            },
        })
        .then(res => res.json())
        .then(data => {
            setName(data.name);
            setDescription(data.description);
            setPrice(data.price);
        })
    }, [])

    return (
      
        <div className="d-flex justify-content-around">
             <Card style={{ position: 'absolute', left: '50%', top: '50%',
            transform: 'translate(-50%, -50%)',
            width: '24rem',
            height: '26rem', 
            }} 
            className="border border-dark">
             <Container>    
                <Form onSubmit={(e) => updateProduct(e)}>

                <h3 className="p-3" style={{textAlign: "center", fontWeight: 'bold'}}> Update Product </h3>
                  <Form.Group className="mb-3" controlId="name">
                    <Form.Label>Product Name</Form.Label>
                    <Form.Control 
                    type="name" 
                    value={name}
                    onChange={(e) => {setName(e.target.value)}}
                    placeholder="Product Name" />
                  </Form.Group>

                  <Form.Group className="mb-3" controlId="description">
                    <Form.Label>Description</Form.Label>
                    <Form.Control type="description"
                    as="textarea" 
                    row={3}
                    value={description}
                    onChange={(e) => {setDescription(e.target.value)}}
                    placeholder="Description" />
                  </Form.Group>

                  <Form.Group className="mb-3" controlId="price">
                    <Form.Label>Price</Form.Label>
                    <Form.Control 
                    type="text"
                    value={price}
                    onChange={(e) => {setPrice(e.target.value)}}
                    placeholder="Price" />
                  </Form.Group>

                  <div className="d-flex justify-content-around">  
                  <Button variant="primary" type="submit">
                    Add
                  </Button>
                  </div>
                </Form>
            </Container>
            </Card>
        </div>      
    )
}