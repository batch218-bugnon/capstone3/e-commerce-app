import { Row, Col, Button, Container, Card} from 'react-bootstrap';

import { useState, useEffect, useContext} from 'react';

import UserContext from '../UserContext';

import {useParams, useNavigate, Link} from 'react-router-dom';

import Swal from 'sweetalert2';



export default function ProductView () {

const {user} = useContext(UserContext);

const {productId} = useParams();

const [name, setName] = useState("");
const [description, setDescription] = useState("");
const [price, setPrice] = useState(0);
const [quantity] = useState(1);



const navigate = useNavigate();

function buyNow (productId) {
  fetch(`${process.env.REACT_APP_API_URL}/order/new-order`, {
    method: "POST",
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${localStorage.getItem('token')}`
    },
    body: JSON.stringify ({
      productId: productId,
      quantity: quantity
    })
  })
  .then (res => res.json())
  .then (data => {
    console.log(data);

      if(data === true){
        Swal.fire({
          title: "Successfully added",
          icon: "success",
          text: "You have successfully added the item to your cart."
        })

        navigate("/products");

        } else {
          Swal.fire({
          title: "Something went wrong",
          icon: "error",
          text: "Please try again."
        })
      }

    
  })
}

useEffect(() => {
  console.log(productId)
    fetch(`${process.env.REACT_APP_API_URL}/product/${productId}?{quantity=${quantity}}`)
      .then(res => res.json())
      .then(data => {
            console.log(data);
    

      setName(data.name);
      setDescription(data.description);
      setPrice(data.price);   

        })

    }, [productId])



return (
    <Container>
            <Row>
                <Col lg={{span: 6, offset:3}} >
                    <Card style = {{
                      position: 'absolute', left: '50%', top: '50%',
                      transform: 'translate(-50%, -50%)',
                      width: '24rem',
                      height: '20rem',
                      }}  
                      className="border border-dark">
                          <Card.Body className="text-center">
                            <Card.Title>{name}</Card.Title>
                            <Card.Subtitle>Description:</Card.Subtitle>
                            <Card.Text>{description}</Card.Text>
                            <Card.Subtitle>Price:</Card.Subtitle>
                            <Card.Text>{price}</Card.Text>
                     
                            {(user.id !== null) ?
                            <Button variant="info" onClick={() => buyNow(productId)}>Buy Now</Button>
                            :
                            <Button variant="info" as={Link} to="/login">Buy Now</Button>
                            }
                          </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>

  )      
}


