import { useState, useEffect, useContext } from 'react';

import { Form, Button, Card, Container } from 'react-bootstrap';

import {Navigate, useNavigate} from 'react-router-dom';

import Swal from 'sweetalert2';


import UserContext from '../UserContext';


export default function Register() {

    const {user} = useContext(UserContext);

    const navigate = useNavigate();

    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [username, setUsername] = useState("");
    const [email, setEmail] = useState("");
    const [password1, setPassword1] = useState("");
    const [password2, setPassword2] = useState("");
    
    const [isActive, setIsActive] = useState(false);


    function registerUser(e) {
        e.preventDefault()

        fetch(`${process.env.REACT_APP_API_URL}/users/checkUsername`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                username:username
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)

            if (data === true) {

                Swal.fire({
                    title: "Username already exist",
                    icon: "error",
                    text: "Kindly user other username."
                })
            } else {
                fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        firstName: firstName,
                        lastName: lastName,
                        username: username,
                        email: email,
                        password: password1
                    })

                })
                .then(res => res.json())
                .then(data => {
                    console.log(data)


                    if(data === true) {

                        setFirstName('');
                        setLastName('');
                        setUsername('');
                        setEmail('');
                        setPassword1('');
                        setPassword2('');


                    Swal.fire({
                            title: "Registration Successful",
                            icon: "success",
                            text: "Welcome to E-commerce App!"
                        })

                        navigate('/login');

                    } else {

                        Swal.fire({
                                title: "Something went wrong",
                                icon: "error",
                                text: "Please, try again."
                            })
                        }
                    })
                }

            })

        }
   


    useEffect(() => {
       
        if((firstName !== '' && lastName !== '' && username !== "" && email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
            setIsActive(true);
        } else {
            setIsActive(false);
        }

    }, [firstName, lastName, username, email, password1, password2])


    return (

    (user.id !== null) ?
    
    <Navigate to="/products" />
    
    :
    
    <div className="d-flex justify-content-around">
         <Card style={{  position: 'absolute', left: '50%', top: '50%',
            transform: 'translate(-50%, -50%)',}}
            className="d-flex justify-content-around border border-dark">
         <Container>
            <Form onSubmit={(e) => registerUser(e)}>
             <h3 className='p-3' style={{textAlign: "center", fontWeight: 'bold',}}>Create an account</h3>
                 <Form.Group controlId="firstName">
                    <Form.Label>First Name</Form.Label>
                    <Form.Control 
                        type="firstName" 
                        placeholder="Enter First Name" 
                        value={firstName}
                        onChange={e => setFirstName(e.target.value)}
                        required
                    />
                </Form.Group>
                 <Form.Group controlId="lastName">
                    <Form.Label>Last Name</Form.Label>
                    <Form.Control 
                        type="lastName" 
                        placeholder="Enter Last Name" 
                        value={lastName}
                        onChange={e => setLastName(e.target.value)}
                        required
                    />
                </Form.Group>
                 <Form.Group controlId="username">
                    <Form.Label>Username</Form.Label>
                    <Form.Control 
                        type="username" 
                        placeholder="Enter Username" 
                        value={username}
                        onChange={e => setUsername(e.target.value)}
                        required
                    />
                </Form.Group>
                <Form.Group controlId="userEmail">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control 
    	                type="email" 
    	                placeholder="Enter email" 
                        value={email}
                        onChange={e => setEmail(e.target.value)}
    	                required
                    />
                </Form.Group>

                <Form.Group controlId="password1">
                    <Form.Label>Password</Form.Label>
                    <Form.Control 
    	                type="password" 
    	                placeholder="Password" 
                        value={password1}
                        onChange={e => setPassword1(e.target.value)}
    	                required
                    />
                </Form.Group>

                <Form.Group controlId="password2">
                    <Form.Label>Verify Password</Form.Label>
                    <Form.Control 
    	                type="password" 
    	                placeholder="Verify Password" 
                        value={password2}
                        onChange={e => setPassword2(e.target.value)}
    	                required
                    />
                </Form.Group>
                
                 <p id="paragraph"> By continuing, you agree to E-commerce App's terms and conditions.</p>

                 <div className="d-flex justify-content-around p-1">
                    { isActive ? 
                    <Button variant="primary" type="submit" id="submitBtn">
                    Sign up
                    </Button>
                    :
                    <Button variant="primary" type="submit" id="submitBtn" disabled>
                    Sign up
                    </Button>
                    }
                </div>
                
            </Form>
        </Container>
        </Card>
    </div>    
    )

}