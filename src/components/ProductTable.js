import { useState, useEffect } from 'react';
import { Table, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function ProductTable() {
	const [products, setProducts] = useState([]);
   

    const archiveProduct = (productId, isActive) => {
        fetch(`${process.env.REACT_APP_API_URL}/product/archive/${productId}`, {
            method: "PATCH",
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem("token")}`
            },
            body: JSON.stringify({
                isActive: !isActive
                
            })
        })
        .then(res => res.json())
        .then(data => {
            if(data) {
                Swal.fire({
                    title: 'Success!',
                    text: `Product successfully ${isActive ? "disabled" : "enabled"}`,
                    icon: 'success',
                    confirmButtonText: 'OK'
                });
                fetchProducts();
            }else{
                Swal.fire({
                    title: 'Error!',
                    text: 'Something went wrong',
                    icon: 'error',
                    confirmButtonText: 'OK'
                });
            }
        })
    }

    const fetchProducts = () => {
        fetch(`${process.env.REACT_APP_API_URL}/product/allproducts`)
		.then(res => res.json())
		.then(data => {
            setProducts(data)
        })
    }

	useEffect(() => {
		fetchProducts();
	}, []);

   
	return (
        <Table striped bordered>
            <thead>
                <tr>
                    <th>Product Name</th>
                    <th>Price</th>
                    <th>Status</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                {
                    products.map((product) => {
                        return (
                            <tr key={product._id}>
                                <td>{product.name}</td>
                                <td>{product.price}</td>
                                <td>
                                    {(product.isActive) ? 
                                        <Button variant="warning" size="sm" onClick={() => archiveProduct(product._id, product.isActive)}>Disable</Button> : 
                                        <Button variant="success" size="sm" onClick={() => archiveProduct(product._id, product.isActive)}>Enable</Button>
                                    }
                                </td>
                                <td>
                                    {/* Add buttons to enable or disable the product. */}
                                    <Button variant="primary" size="sm" as={Link} to={`/product/update/${product._id}`}>Update</Button>
                                </td>
                            </tr>
                        )
                    })
                }
            </tbody>
        </Table>    
    );
}
