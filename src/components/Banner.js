// import Button from 'react-bootstrap/Button';
// import Row from 'react-bootstrap/Row';
// import Col from 'react-bootstrap/Col';

import { Button, Row, Col } from 'react-bootstrap';

export default function Banner({data}) {

    const {title, label} = data;

return (
    <Row>
    	<Col className="p-5">
            <h1>{title}</h1>
            <Button variant="info">{label}</Button>
        </Col>
    </Row>
	)
}