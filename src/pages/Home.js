import {Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import { useContext } from 'react';
import UserContext from '../UserContext';
import Highlights from '../components/Highlights';


export default function Home() {
    const { user } = useContext(UserContext);

    return (
        <>
            <h1 className="p-3">E-commerce App</h1>
          
                {(user.isAdmin !== true) ?
                    <>
                    <Button className="m-3" variant="primary" as={Link} to={'/products'}>Buy Now</Button>
                    <Highlights />
                    </>
                    :
                    <>
                    <Button className="m-3" variant="primary" as={Link} to={'/add-product'}>Add Product</Button>
                    <Highlights />
                    </>
                }  
        </>
           

    )
}
