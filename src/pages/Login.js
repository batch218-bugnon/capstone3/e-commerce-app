import {useState, useEffect, useContext} from 'react'
import {Form, Button, Card, Container} from 'react-bootstrap'
import {Navigate, Link} from 'react-router-dom'

import Swal from 'sweetalert2';

import UserContext from '../UserContext';



export default function Login(){

    const {user, setUser} = useContext(UserContext);

    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');

    const [isActive, setIsActive] = useState(false)

    function Login(e) {
        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                username: username,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            // We will receive either a token or a false response.
            console.log(data);

            if(typeof data.access !== "undefined") {
                localStorage.setItem('token', data.access)
    
                retrieveUserDetails(data.access);


                Swal.fire({
                  icon: 'success',
                  title: 'Login Successul',
                  text: 'Welcome to E-commerce App!'
                })

            } else {

                Swal.fire({
                  icon: 'error',
                  title: 'Oops...',
                  text: 'Something went wrong!'
                })
            };

        });

        setUsername('');
        setPassword('');

    };


    const retrieveUserDetails = (token) => {

        fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {
            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })
        })
    };

    useEffect(() => {
        if((username !== '' && password !== '')){
            setIsActive(true);
        } else {
            setIsActive(false);
        }
    }, [username, password])

    return(


        (user.id !== null) ?

        <Navigate to="/" />
        :
        <div className="d-flex justify-content-center">
         <Card style = {{
            position: 'absolute', left: '50%', top: '50%',
            transform: 'translate(-50%, -50%)',
            width: '24rem',
            height: '20rem',
            }} 
            className="border border-dark">
         <Container>
            <Form onSubmit={e => Login(e)}>
                <h3 className="p-3" style={{textAlign: "center", fontWeight: 'bold' }}> Sign in </h3>
                <Form.Group controlId="username">
                    <Form.Label>Username</Form.Label>
                    <Form.Control 
                        size="sm"
                        type="username" 
                        placeholder="Enter username"
                        value={username}
                        onChange={e => setUsername(e.target.value)}
                        required
                    />
                </Form.Group>

                <Form.Group controlId="password">
                    <Form.Label>Password</Form.Label>
                    <Form.Control 
                        size="sm"
                        type="password" 
                        placeholder="Password"
                        value={password}
                        onChange={e => setPassword(e.target.value)}
                        required
                    />
                </Form.Group>
                <div mt-3>
                <p className='pt-3'> Not registered? <Link to="/register">Create an account</Link></p>
                </div>
                <div className="d-flex justify-content-around">
                {   isActive ?
                    <Button variant="primary" type="submit" id="submitBtn">
                        Sign in
                    </Button>
                    :
                    <Button variant="primary" type="submit" id="submitBtn" disabled>
                        Sign in
                    </Button>
                }
               </div>
            </Form>
         </Container>
         </Card>
        </div>
    )
};