import { useContext } from 'react';
import { Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import ProductCards from '../components/ProductCards';

import ProductTable from '../components/ProductTable';

// import Dashboard from '../components/Dashboard';
// import Banner from '../components/Banner';
// import DashboardHeader from '../components/DashboardHeader';

import UserContext from '../UserContext';

export default function ProductsPage() {
    const { user } = useContext(UserContext);

    return (
        <>
            <h1 className="pt-3">Products</h1>
            {(user.isAdmin) ? (
                <>
                    <div className='pt-3'>
                    <Button className="mb-3" variant="primary" as={Link} to={'/add-product'}>Add Product</Button>
                    <ProductTable/>
                    </div>
                </>
            ) : (
                <ProductCards/>
            )}
        </>
    )
}
