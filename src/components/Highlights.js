import {Row, Col , Card} from 'react-bootstrap';

export default function Highlights() {
    return (
        <Row className="mt-3 mb-3">
            <Col xs={12} md={4}>
                <Card className="cardHighlight p-4 border border-dark">
                    <Card.Body>
                        <Card.Title>
                            <h2>dolor</h2>
                        </Card.Title>
                        <Card.Text>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut placerat orci nulla pellentesque dignissim enim sit amet venenatis. Ut consequat semper viverra nam libero justo laoreet sit amet.
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
            <Col xs={12} md={4}>
                <Card className="cardHighlight p-4 border border-dark">
                    <Card.Body>
                        <Card.Title>
                            <h2>tincidunt</h2>
                        </Card.Title>
                        <Card.Text>
                           Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Mi quis hendrerit dolor magna eget est lorem ipsum dolor. Vestibulum morbi blandit cursus risus at ultrices.
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
            <Col xs={12} md={4}>
                <Card className="cardHighlight p-4 border border-dark">
                    <Card.Body>
                        <Card.Title>
                            <h2>pulvinar</h2>
                        </Card.Title>
                        <Card.Text>
                           Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Placerat orci nulla pellentesque dignissim. Id aliquet risus feugiat in.
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
        </Row>
    )
}