import { useState, useEffect } from 'react';
import { Card, Row, Col, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';




export default function ProductCards() {
	const [products, setProducts] = useState([]);
 
    

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/product/allproducts`)
		.then(res => res.json())
		.then(data => {
            setProducts(data)
        })
	}, []);


    return (
        <Row className="mt-3 mb-3 g-3">
            {
                products.map((product) => {
                    return (
                        <Col xs={6} md={3} lg={3}>
                            <Card className="cardHighlight p-4 border border-dark">
                                <Card.Body>
                                    <Card.Title>
                                        <h2>{product.name}</h2>
                                    </Card.Title>

                                    <Card.Subtitle>Description</Card.Subtitle>
                                    <Card.Text>
                                        {product.description}
                                    </Card.Text>

                                    <Card.Subtitle>Price:</Card.Subtitle>
                                    <Card.Text>
                                        {product.price}
                                    </Card.Text>

                                    <Button variant="info" as={Link} to ={`/product/${product._id}`}>See Details</Button>
                                </Card.Body>
                            </Card>
                        </Col>
                    )
                })
            }
         </Row>   
    );
}
