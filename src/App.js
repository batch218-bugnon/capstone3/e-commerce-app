import { useState, useEffect } from 'react';

import { Container } from 'react-bootstrap';

import {UserProvider} from './UserContext';

import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';

import AppNavbar from './components/AppNavbar';

 import ProductView from './pages/ProductView';

 import AddProduct from './pages/AddProduct';
 import UpdateProductPage from './pages/UpdateProductPage';
 import ProductsPage from './pages/ProductsPage';

 import Login from './pages/Login';
 import Register from './pages/Register';
 import Logout from './pages/Logout';

import Home from './pages/Home';






import './App.css';

function App() {

    const [user, setUser] = useState({
      id: null,
      isAdmin:null
  });

    const unsetUser = () => {
      setUser({
        id: null,
        isAdmin: null
      })
    }
  
  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {
      console.log('data', data)

      if (typeof data._id !== "undefined") {
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      } else {
        setUser({
          id: null,
          isAdmin: null
        })
      }


    })
  }, []);

  return (
  
    <>
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <AppNavbar/>
        <Container>
          <Routes>
              <Route path="/" element={<Home/>}/>
             
              <Route path="/product/:productId" element={<ProductView/>}/>

             
              <Route path="/add-product" element={<AddProduct/>}/>
              <Route path="/product/update/:productId" element={<UpdateProductPage/>}/>
              <Route path="/products" element={<ProductsPage/>}/>

            
              <Route path="/login" element={<Login/>}/>
              <Route path="/register" element={<Register/>}/>
              <Route path="/logout" element={<Logout/>}/>
              
          </Routes>
        </Container>
      </Router>
    </UserProvider>
    </>

  );
}

export default App;



