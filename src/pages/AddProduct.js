import {useState, useEffect} from 'react';

import {useNavigate} from 'react-router-dom';

import {Button, Form, Container} from 'react-bootstrap';

import Swal from 'sweetalert2';


export default function AddProduct(){

  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState("");

  const [isActive, setIsActive] = useState(false);


  const navigate = useNavigate();


  function AddProduct(e) {

      e.preventDefault()

      fetch(`${process.env.REACT_APP_API_URL}/product/addproduct`, {
        method: "POST",
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${localStorage.getItem("token")}`
        },
        body: JSON.stringify({
            name: name,
            description: description,
            price: price
        })
    })
    .then(res => res.json())
    .then(data => {
        console.log(data)

        if(data === true) {
            setName("");
            setDescription("");
            setPrice("");


        Swal.fire({
                title: "Product Successfully Added",
                icon: "success",
                text: "Add another one!"

            })

            navigate("/products");

        } else {

        Swal.fire({
                title: "Something went wrong",
                icon: "error",
                text: "Please try again!"
            })
        }
    })
  }

  useEffect(() => {

      if((name !== '' && description !== '' && price !== '')){
          setIsActive(true);
      } else {
          setIsActive(false);
      }
  }, [name, description, price])



  return (
    <div className="d-flex justify-content-around">
      <Container style={{ position: 'absolute', left: '50%', top: '50%',
            transform: 'translate(-50%, -50%)',
            width: '24rem',
            height: '27rem', 
            }} 
            className="border border-dark">	
        <Form onSubmit={(e) => AddProduct(e)}>

        <h3 className="p-3" style={{textAlign: "center", fontWeight: 'bold' }}> Add Product </h3>
          <Form.Group className="mb-3" controlId="name">
            <Form.Label>Product Name</Form.Label>
            <Form.Control 
              type="name" 
              value={name}
              onChange={(e) => {setName(e.target.value)}}
              placeholder="Product Name"
            />
          </Form.Group>

          <Form.Group className="mb-3" controlId="description">
            <Form.Label>Description</Form.Label>
            <Form.Control 
              as="textarea" 
              type="description" 
              rows={3}
              value={description}
              onChange={(e) => {setDescription(e.target.value)}}
              placeholder="Description"
            />
          </Form.Group>

          <Form.Group className="mb-3" controlId="price">
            <Form.Label>Price</Form.Label>
            <Form.Control 
              type="number"
              value={price}
              onChange={(e) => {setPrice(e.target.value)}}
              placeholder="Price"
            />
          </Form.Group>

          <div className="d-flex justify-content-around">
            <Button variant="success" type="submit" disabled={!isActive}>Add</Button>
          </div>
        </Form>
      </Container>
    </div>	    
  );
}  