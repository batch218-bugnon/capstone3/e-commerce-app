import {useContext} from 'react';

import { Link, NavLink } from 'react-router-dom';

import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import Container from 'react-bootstrap/Container';

import UserContext from '../UserContext';

export default function AppNavbar(){


  const {user} = useContext(UserContext);

  console.log('appnavbar', user.id)

  return(
    <Navbar bg="dark" variant="dark" expand="lg">
      <Container fluid>
        <Navbar.Brand as={Link} to="/">E-Commerce App</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav"/>
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="justify-content-end ms-auto">
            <Nav.Link as={NavLink} to="/">Home</Nav.Link>
          
            { (user.isAdmin === true) ?
            <Nav.Link as={NavLink} to="/products">Product List</Nav.Link>
            : 
            <Nav.Link as={NavLink} to="/products">Products</Nav.Link>
          }
           
            
            { (user.id !== null) ?
              <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
              :
              <>
                <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
              </>
            }
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  )
}


