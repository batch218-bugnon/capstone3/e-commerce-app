import {Table, Button} from 'react-bootstrap';
import {FontAwesomeIcon, icon} from '@fortawesome/react-fontawesome';

import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';

export default function Dashboard({product}) {
    console.log(product)

    const {name, description, price, _id} = product;




    const archiveProduct = (productId, isActive) => {
        fetch(`${process.env.REACT_APP_API_URL}/product/:productId/archive`, {
            method: "PATCH",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${localStorage.getItem("token")}`
            },
            body: JSON.stringify({
                isActive: !isActive
            })
        })
        .then(res => res.json())
        .then(data => {
            if(data){
                let status

                isActive ? 

                    status = "disabled" 
                    : 
                    status = "enabled"

                alert(`Product successfully ${status}`)

            
            }else{
                alert("Something went wrong")
            }
        })
    }

    

    return (
        <div>
            <Table striped>
                <tbody>
                    <tr>
                        <td>{name}</td>
                        <td>{description}</td>
                        <td>{price}</td>
                        <td>

                           {(product.isActive) ? 
                            <Button variant="danger" size="sm" onClick={() => archiveProduct(product._id, product.isActive)}>Disable</Button>
                            : <Button variant="success" size="sm" onClick={() => archiveProduct(product._id, product.isActive)}>Enable</Button>
                            }
       
                            
                        </td>
                        <td>
                            <Button variant="info" as={Link} to={`update/${_id}`}>Update</Button>{' '}
                        </td>
                    </tr>
                </tbody>
            </Table>    
        </div>  
    );
}


Dashboard.propTypes = {
    product: PropTypes.shape({
        name: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired
    })
}