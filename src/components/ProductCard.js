// import {Row	, Button, Card, Col} from 'react-bootstrap';

// import {Link} from 'react-router-dom';

// import PropTypes from 'prop-types';



// export default function ProductCard({product}) {

//     const { name, description, price, _id} = product;

//     return (
//          <Row className="mt-3 mb-3">
//             <Col xs={12} md={3} lg={{span: 6, offset: 3}}>
//                 <Card className="cardHighlight p-3">
//                     <Card.Body>
//                         <Card.Title>
//                             <h2>{name}</h2>
//                         </Card.Title>

//                         <Card.Subtitle>Description</Card.Subtitle>
//                         <Card.Text>
//                             {description}
//                         </Card.Text>

//                         <Card.Subtitle>Price:</Card.Subtitle>
//                         <Card.Text>
//                          {price}
//                         </Card.Text>

//                         <Button variant="info" as={Link} to ={`/product/${_id}`}>See Details</Button>

//                     </Card.Body>
//                 </Card>
//             </Col>
//         </Row>   
//     )
// }


// ProductCard.propTypes = {
//     product: PropTypes.shape({
//         name: PropTypes.string.isRequired,
//         description: PropTypes.string.isRequired,
//         price: PropTypes.number.isRequired
//     })
// }